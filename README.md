[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.stud.idi.ntnu.no/tdt4100/videoer)

# videoer

Repo med kode laget ved tagning av forelesningsvideoer. Mer om hvert eksempel ligger i de enkelte pakkene:

- [delegering](./videoer/src/delegering/README.md)
- [delegering2](videoer/src/delegering2/README.md)
- [observerbar](videoer/src/observerbar/README.md)
- [observerbar2](videoer/src/observerbar2/README.md)
- [arv](videoer/src/arv/README.md)
- [arv2](videoer/src/arv2/README.md)
