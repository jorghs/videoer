package delegering;

public class ValueAverager {

	public ValueAverager() {
		valueSource = new ValueSource(getClass().getResourceAsStream("values.txt"));
		valueComputer = new AverageComputer();
	}

	private ValueSource valueSource;
	private ValueComputer valueComputer;

	public boolean hasNextValue() {
		return valueSource.hasNextValue();
	}

	public double nextValue() {
		double value = valueSource.nextValue();
		valueComputer.acceptValue(value);
		return value;
	}

	public double getAverage() {
		return valueComputer.computeValue();
	}

	/*
	 * @startuml
	 * object ValueAverager {
	 * 	count = 0
	 *  sum = 0.0
	 * }
	 * object ValueSource
	 * object ValueComputer
	 * object Random
	 * ValueAverager --> ValueSource: valueSource
	 * ValueSource --> Random: random
	 * ValueAverager --> ValueComputer: valueComputer
	 * @enduml
	 *
	 * @startuml
	 * main -> ValueAverager: nextValue()
	 * ValueAverager -> ValueSource: nextValue()
	 * ValueSource -> Random: nextDouble()
	 * Random --> ValueSource: <some value>
	 * ValueSource --> ValueAverager: <some value>
	 * ValueAverager --> main: <some value>
	 * main -> ValueAverager: getAverage()
	 * ValueAverager -> ValueComputer: computeValue
	 * ValueComputer --> ValueAverager: <some value>
	 * ValueAverager --> main: <some value>
	 * main -> ValueAverager: ...
	 *
	 * @enduml
	 */
	public static void main(final String[] args) {
		final ValueAverager valueAverager = new ValueAverager();
		while (valueAverager.valueSource.hasNextValue()) {
			valueAverager.nextValue();
			System.out.println(valueAverager.getAverage());
		}
	}
}
