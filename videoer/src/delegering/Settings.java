package delegering;

import java.util.HashMap;
import java.util.Map;

public class Settings {

	private final Map<String, Object> settings = new HashMap<>();

	public boolean hasSetting(final String settingName) {
		return settings.containsKey(settingName);
	}

	public Object getSetting(final String settingName) {
		return settings.get(settingName);
	}

	public void updateSetting(final String settingName, final Object value) {
		settings.put(settingName, value);
	}

	/*
	 * @startuml
	 * main -> Settings: hasSetting("theme")
	 * Settings -> "settings: Map" as settings: containsKey("theme")
	 * settings --> Settings: false
	 * Settings --> main: false
	 * main -> Settings: updateSetting("theme", "dark")
	 * Settings -> settings: put("theme", "dark")
	 *
	 * main -> Settings: hasSetting("theme")
	 * Settings -> settings: containsKey("theme")
	 * settings --> Settings: true
	 * Settings --> main: true
	 * main -> Settings: getSetting("theme")
	 * Settings -> settings: get("theme")
	 * settings --> Settings: "dark"
	 * Settings --> main: "dark"
	 * @enduml
	 */
	public static void main(final String[] args) {
		final Settings settings = new Settings();
		System.out.println(settings.hasSetting("theme"));
		settings.updateSetting("theme", "dark");
		System.out.println(settings.hasSetting("theme"));
		System.out.println(settings.getSetting("theme"));
	}
}
