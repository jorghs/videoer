package delegering;

public class AverageComputer implements ValueComputer {

	private int count = 0;
	private double sum = 0.0;

	@Override
	public void acceptValue(double value) {
		sum += value;
		count++;
	}

	@Override
	public double computeValue() {
		return sum / count;
	}
}
