package arv2;

public class PointNd extends AbstractPoint {

	private double[] coords;
	
	public PointNd(double... coords) {
		this.coords = coords;
	}
	
	@Override
	public int coordCount() {
		return coords.length;
	}

	@Override
	public double getCoord(int num) {
		return coords[num];
	}

	public static void main(String[] args) {
		PointNd p1 = new PointNd(1.0, 2.0, 3.0, 4.0);
		Point2d p2 = new Point2d(1.0, 2.0);
		Point3d p3 = new Point3d(1.0, 2.0, 3.0);
		Point2d p4;
		Point3d p5;
		p4 = p3;
		if (p4 instanceof Point3d) {
			p5 = (Point3d) p4;
		}
		System.out.println(p2.distance());
	}
}
