# Arv i klassehierari

I dette eksemplet ser vi på et arvingshierarki, med en abstrakt klasse på toppen og flere konkrete klasser under.

```plantuml
abstract class AbstractPoint {
	{abstract} int coordCount()
	{abstract} double getCoord(int num)

	double distance()
	double dot(AbstractPoint other)
}
class Point2d extends AbstractPoint {
	int coordCount()
	double getCoord(int num)
}
class Point3d extends Point2d {
	int coordCount()
	double getCoord(int num)
}
class PointNd extends AbstractPoint {
	int coordCount()
	double getCoord(int num)
}
```

- [AbstractPoint](AbstractPoint.java) er en abstrakt superklasse som inneholder felles funksjonalitet for de andre klassene. 
- [Point2d](Point2d.java) representerer et to-dimensjonalt punkt med x- og y-koordinater.
- [Point3d](Point3d.java) representerer et tre-dimensjonalt punkt med x-, y- og z-koordinater.
- [PointNd](PointNd.java) representerer et n-dimensjonalt punkt, hvor n bestemmes av antall koordinater som oppgis i konstruktøren.

## AbstractPoint

- [AbstractPoint](AbstractPoint.java) inneholder to generelle metoder, `distance` (fra origo) og `dot` (produkt), som virker uavhengig av antall koordinater (dimensjoner). Disse trenger/bruker to metoder, `coordCount` og `getCoord`, som defineres som abstrakte, i og med at de ikke (kan) implementes her. I stedet må de implementeres i alle (konkrete) subklasser.

## Point2d

Lagrer sine koordinater i `x`- og `y`-feltene og implementerer `coordCount` og `getCoord` basert på disse.
Implementerer (redefinerer) `toString` og `equals`-metodene fra `Object`. `toString` bruker en hjelpemetode som er laget med tanke på evt. subklasser.
`equals`-metoden tar høyde for at argumentet kan være en instans av en subklasse. Et slikt objekt kan jo ikke være `equals` et `Point2d`-objekt.

## Point3d

Klassen arver fra [Point2d](Point2d.java) og prøver å bruke arvede metoder i egen implementasjon av `toString` og `equals`. 
Lagrer sitt ekstra koordinat i `z`-feltet og implementerer `coordCount` og `getCoord` vha. dette feltet og tilsvarende metoder i superklassen.

## PointNd

Lagrer koordinatene i en tabell (array) implementerer `coordCount` og `getCoord` vha. den.
Vi har ikke implementert (redefinert) `toString` og `equals`-metodene, prøv du!
