package observerbar2;

public class AntiBeundrer implements Fan {

	private String hårfarge = "lys";
	private int hårlengde = 40;

	public String getHårfarge() {
		return hårfarge;
	}

	public void setHårfarge(String hårfarge) {
		this.hårfarge = hårfarge;
	}

	@Override
	public void propertyChanged(Idol idol, String egenskap, Object oldValue, Object newValue) {
		if (Idol.HAIR_COLOR_PROPERTY.equals(egenskap)) {
			setHårfarge("not " + idol.getHairColor());
		} else if (Idol.HAIR_LENGTH_PROPERTY.equals(egenskap)) {
			setHårlengde(100 - idol.getHairLength());
		}
	}

	public int getHårlengde() {
		return hårlengde;
	}

	public void setHårlengde(int hårlengde) {
		this.hårlengde = hårlengde;
	}

	/*
	 * @startuml
	 * class Idol
	 * class Fans
	 * class Beundrer
	 * class AntiBeundrer
	 * Beundrer .up.|> Fans
	 * AntiBeundrer .up.|> Fans
	 * Idol -> "*" Fans: fans
	 * @enduml
	 *
	 * @startuml
	 * object Idol
	 * object Beundrer
	 * object AntiBeundrer
	 * Idol -down-> Beundrer: fans
	 * Idol -down-> AntiBeundrer: fans
	 * @enduml
	 *
	 * @startuml
	 * main -> "idol: Idol" as idol: addFan(beundrer)
	 * main -> "idol: Idol" as idol: addFan(antibeundrer)
	 * main -> idol: setHairColor("red")
	 * idol -> idol: firePropertyChanged("hairColor", "black", "red)
	 * idol -> "beundrer: Beundrer" as beundrer: propertyChanged(idol, "hairColor", "black", "red)
	 * beundrer -> beundrer: setHårfarge("red")
	 * idol -> "antibeundrer: AntiBeundrer" as antibeundrer: propertyChanged(idol, "hairColor", "black", "red)
	 * antibeundrer -> antibeundrer: setHårfarge("not red")
	 * @enduml
	 */
	public static void main(String[] args) {
		Idol idol = new Idol();
		AntiBeundrer antiBeundrer = new AntiBeundrer();
		Beundrer beundrer = new Beundrer();
		idol.addFan(beundrer);
		idol.addFan(antiBeundrer);
		idol.setHairColor("rød");
		System.out.println(idol.getHairColor());
		System.out.println(beundrer.getHårfarge());
		System.out.println(antiBeundrer.getHårfarge());
		idol.setHairLength(80);
		System.out.println(idol.getHairLength());
		System.out.println(beundrer.getHårlengde());
		System.out.println(antiBeundrer.getHårlengde());
	}
}
