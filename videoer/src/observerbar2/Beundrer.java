package observerbar2;

public class Beundrer implements Fan {

	private String hårfarge = "lys";
	private int hårlengde = 40;

	public String getHårfarge() {
		return hårfarge;
	}

	public void setHårfarge(String hårfarge) {
		this.hårfarge = hårfarge;
	}

	@Override
	public void propertyChanged(Idol idol, String property, Object oldValue, Object newValue) {
		if (Idol.HAIR_COLOR_PROPERTY.equals(property)) {
			setHårfarge(idol.getHairColor());
		} else if (Idol.HAIR_LENGTH_PROPERTY.equals(property)) {
			setHårlengde(idol.getHairLength());
		}
	}

	public int getHårlengde() {
		return hårlengde;
	}

	public void setHårlengde(int hårlengde) {
		this.hårlengde = hårlengde;
	}

	/*
	 * @startuml
	 * class Idol
	 * interface Fans
	 * class Beundrer
	 * Beundrer .up.|> Fans
	 * Idol -> "*" Fans: fans
	 * @enduml
	 *
	 * @startuml
	 * object Idol
	 * object Beundrer
	 * Idol -down-> Beundrer: fans
	 * @enduml
	 *
	 * @startuml
	 * main -> "idol: Idol" as idol: addFan(beundrer)
	 * main -> idol: setHairColor("red")
	 * idol -> idol: firePropertyChanged("hairColor", "black", "red)
	 * idol -> "beundrer: Beundrer" as beundrer: propertyChanged(idol, "hairColor", "black", "red)
	 * beundrer -> beundrer: setHårfarge("red")
	 * @enduml
	 */
	public static void main(String[] args) {
		Idol idol = new Idol();
		Beundrer beundrer = new Beundrer();
		idol.addFan(beundrer);
		idol.setHairColor("red");
		System.out.println(idol.getHairColor());
		System.out.println(beundrer.getHårfarge());
		idol.setHairLength(80);
		System.out.println(idol.getHairLength());
		System.out.println(beundrer.getHårlengde());
	}
}
