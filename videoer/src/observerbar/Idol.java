package observerbar;

import java.util.ArrayList;
import java.util.Collection;

public class Idol {

	private String hårfarge = "svart";
	private int hårlengde = 40;

	private Collection<Fan> fans = new ArrayList<>();

	public void addFan(Fan Fan) {
		this.fans.add(Fan);
	}

	public void removeFan(Fan Fan) {
		this.fans.remove(Fan);
	}

	public String getHårfarge() {
		return hårfarge;
	}

	protected void siFraOmEndring(String egenskap) {
		for (Fan fan : fans) {
			fan.idolErEndret(this, egenskap);
		}
	}

	public void setHårfarge(String hårfarge) {
		this.hårfarge = hårfarge;
		siFraOmEndring("hårfarge");
	}

	public int getHårlengde() {
		return hårlengde;
	}

	public void setHårlengde(int hårlengde) {
		this.hårlengde = hårlengde;
		siFraOmEndring("hårlengde");
	}
}
