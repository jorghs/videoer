# Delegering 2

Denne pakken inneholder en videreføring og opprydding av App-innstillinger-eksemplet i [delegeringspakken](../delegering/README.md).

Her er det innført et [ISettings](ISettings.java)-grensesnitt, som de andre to klassene (som har fått nye navn) implementerer.
